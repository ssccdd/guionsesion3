[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# GUIÓN DE PRÁCTICAS[^nota1]
## Sesión 3: Utilidades para la Sincronización de Hilos

Una de las situaciones más comunes en la programación concurrente es cuando los hilos deben compartir recursos. En las aplicaciones concurrentes, es común que diferentes hilos accedan a las mismas variables o accedan al mismo archivo o tengan acceso a la misma base de datos. El acceso concurrente podría provocar **inconsistencia en los datos** y debemos tener mecanismos a nuestra disposición que eviten estos errores.

En las clases teóricas de la asignatura trataremos este problema, que se conoce como **sección crítica**, con más detalle. De forma resumida, una **sección crítica** es un bloque de código que tienen acceso a recursos compartidos entre diferentes hilos y que sólo podrá ser ejecutarlo uno de ellos a la vez. 

Java nos proporciona un elemento básico para [**sincronizar**](https://gitlab.com/ssccdd/materialadicional/-/blob/master/README.md) los hilos para la programación concurrente. Por medio de estos elementos de sincronización se puede garantizar que sólo un hilo esté ejecutando el código comprendido en una **sección crítica**. De esta forma, el resto de hilos que intenten acceder a la sección crítica suspenderán su ejecución mientras esté ya un hilo ejecutando esa sección crítica. 

En este guión se va a presentar otra herramienta que nos proporciona Java, por medio de una serie de ejemplos, que garantizará la sincronización de los hilos: la utilización de la interface [`Lock`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/Lock.html) y sus implementaciones:

1. [Sincronizando un bloque de código con la interface `Lock`.](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio1.md)
2. [Sincronizando el acceso a los datos mediante la interface `ReadWriteLock`.](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio2.md)
3. [Utilizando múltiples condiciones en un Bloqueo.](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio3.md)

En los ejemplos anteriores, aprendimos los conceptos de **sincronización** y **sección crítica**. Básicamente, se presentaba la sincronización cuando una o más tareas concurrentes comparten un recurso, por ejemplo, un objeto o un atributo de un objeto. Al conjunto de sentencias que permiten el acceso a ese recurso compartido se definió como la **sección crítica**. Si no utilizamos los elementos apropiados, podemos encontrarnos con resultados erróneos, inconsistencia en los datos, etc … Por tanto, utilizamos unos primeros mecanismos que nos proporciona Java para evitar esos problemas y garantizar el acceso a la **sección crítica**.

Ahora utilizaremos mecanismos de *alto nivel* para **sincronizar múltiples hilos**. Estos mecanismos de *alto  nivel* son los siguientes:

- [`CountDownLatch`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CountDownLatch.html): Esta clase proporciona un mecanismo que permite a un hilo esperar hasta la finalización de múltiples operaciones.

- [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html): Esta clase proporciona un mecanismo que permite la sincronización de múltiples hilos en un punto en común.

- [`Phaser`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html): Esta clase tiene un mecanismo que controla la ejecución concurrente de tareas divididas en etapas. Todos los hilos deberán terminar una etapa antes de continuar con la siguiente. Es una característica nueva desde el API de Java 7.

Para demostrar su utilización se presentan los siguientes ejemplos:

4. [Esperando a múltiples eventos concurrentes.](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio4.md)
5. [Sincronizando tareas en un punto en común](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio5.md).
6. [Ejecución de tareas por etapas concurrentes.](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio6.md)
7. [Controlando el cambio de etapa en tareas por etapas concurrentes.](https://gitlab.com/ssccdd/guionsesion3/-/blob/master/Ejercicio7.md)

---
[^nota1]: El guión se extrae del libro *Java 7 Concurrency Cookbook* que se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIxNjc3MzA1Myw5Nzc0NjIyOTMsMTc4Mz
czMzIzOCwxMzczNTAzMDQ4LDEyMzkxNzkxMDFdfQ==
-->