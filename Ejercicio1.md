# Sincronizando un bloque de código con la interface `Lock`

En Java tenemos un mecanismo que está basado en la interface [`Lock`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/Lock.html) y las clases que la implementan, como [`ReentrantLock`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/ReentrantLock.html). Las ventajas de este mecanismo son las siguientes:

-   Podemos estructurar los **bloques sincronizados** de una manera más flexible. Con la palabra reservada `synchronized` el programador deberá obtener y liberar el control del bloque sincronizado como parte del código. La interface `Lock` permite tener estructuras más complejas para implementar las **secciones críticas**.
    
-   La interface `Lock` proporciona funcionalidades adicionales sobre la palabra reservada `synchronized`. Una de esas nuevas funcionalidades es implementada por el método `tryLock()`. Con este método se intenta ver si el bloque sincronizado se encuentra libre. Con el mecanismo `synchronized`, si ya hay un hilo ejecutando el bloque, el nuevo hilo que lo intente quedará bloqueado. El método `tryLock()`  devolverá un valor `boolean` indicando si ya hay otro hilo en el **bloque sincronizado**.
    
-   La interface `Lock` permite separar las acciones de lectura de las de escritura, es decir, se permite que la lectura sea concurrente mientras que la escritura se realice en exclusión mutua. Esto es, da una implementación para el problema del **Lector/Escritor**[^nota1].
    
-   La interface `Lock` es más eficiente que el mecanismo `synchronized`.
    
En el ejemplo se mostrará el uso de la interface `Lock` para establecer la **sección crítica** mediante la clase `ReentrantLock` que la implementa. El ejemplo simulará una cola de impresión.

1. Definimos una clase llamada `PrintQueue` para implementar la cola de impresión. Creamos un atributo constante para un objeto que implementa la interface `Lock`, en este caso, de la clase `ReentrantLock`.

```java
...
public class PrintQueue {

    /**
     * Lock to control the access to the queue.
     */
    private final Lock queueLock=new ReentrantLock();
...
```

2. Implementamos el método `printJob(.)`. El método simulara que imprimirá un trabajo que se le pase como parámetro.

```java
...
/**
 * Method that prints a document
 * @param document document to print
 */
public void printJob(Object document){
    queueLock.lock();
...
```

3. Como sólo un trabajo puede ser impreso a la vez, lo primero que haremos será garantizar la **exclusión mutua** para realizar el trabajo de impresión. Para ello realizaremos la llamada al método `lock()`.

4. Ahora incluimos el código que simula el trabajo de impresión, la simulación se realizará mediante la suspensión del hilo por un tiempo aleatorio por medio del método `sleep(.)`.

```java
...
try {
    Long duration=(long)(Math.random()*10000);
    System.out.printf("%s: PrintQueue: Printing a Job during %d seconds\n",Thread.currentThread().getName(),(duration/1000));
    Thread.sleep(duration);
} catch (InterruptedException e) {
    e.printStackTrace();
} finally {
    queueLock.unlock();
}
...
```

5. Lo último será liberar el control de la sección crítica para permitir que otro hilo pueda realizar su trabajo de impresión mediante el método `unlock()`. Utilizamos la cláusula `finally`  para garantizar que el control de la sección crítica es liberado. De esta forma evitaremos que se produzcan **deadlocks** si algún hilo es interrumpido antes de finalizar su trabajo de impresión.

6. Definimos una clase llamada `Job` que implementa la interface `Runnable` para simular los trabajos de impresión. Creamos una variable de instancia para la cola de impresión que utilizaremos y que inicializamos con el constructor.

```java
...
public class Job implements Runnable {

    /**
     * Queue to print the documents
     */
    private PrintQueue printQueue;
	
    /**
     * Constructor of the class. Initializes the queue
     * @param printQueue
     */
    public Job(PrintQueue printQueue){
        this.printQueue=printQueue;
    }
...
```

7. El método `run()` representará la tarea de impresión. Presentaremos por la consola un mensaje del trabajo que se va a imprimir, envía el trabajo a la cola de impresión y presenta un mensaje cuando el trabajo ha finalizado.

```java
...
/**
 * Core method of the Job. Sends the document to the print queue and waits
 *  for its finalization
 */
@Override
public void run() {
    System.out.printf("%s: Going to print a document\n",Thread.currentThread().getName());
    printQueue.printJob(new Object());
    System.out.printf("%s: The document has been printed\n",Thread.currentThread().getName());		
}
...
```

8. En el método `main(.)` deberá crear un objeto para la cola de impresión, `PrintQueue`, y 10 objetos de la clase `Thread` para realizar la ejecución de 10 objetos de la clase `Job`. Para finalizar lanzaremos la ejecución de esos 10 hilos.

## Ejercicios propuestos

-   Revisar el libro *Java 7 Concurrency Cookbook*[^nota2] para tener una explicación más detallada sobre el resultado del ejemplo.    
-   Modificar el método `main(.)` para limitar la ejecución de la aplicación a 30 segundos. Se deberá interrumpir la ejecución de los trabajos de impresión que no han finalizado.
-   Realizar un programa que simule un aparcamiento con una capacidad de 100 coches. Los coches entran y salen del aparcamiento. Si el aparcamiento está completo y un coche quiere entrar sólo esperan el 80% de ellos, el resto buscará otro aparcamiento.

---
[^nota1]: Este problema será expuesto en detalle en las clases de teoría.
[^nota2]: El libro se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbNzk1MjQwMDIyLDc2NDAwODExMF19
-->