# Esperando a múltiples eventos concurrentes

En Java disponemos de una clase que permite que uno o más hilos esperen hasta que un conjunto de operaciones se hayan realizado, la clase es [`CountDownLatch`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CountDownLatch.html). En el constructor se pasa un número entero, que representa el número de operaciones que los hilos deberán esperar que se completen. Cuando un hilo quiere esperar hasta que se complete la ejecución de las operaciones invocará el método `await()`. Este método suspenderá la ejecución del hilo hasta que todas las operaciones estén completadas. Cuando una de esas operaciones ha finalizado se invocará el método `countDown()` que decrementa el contador interno de `CountDownLatch`. Cuando el contador llega a `0` se reanudará la ejecución de todos los hilos suspendidos por el método `await()`.

En el ejemplo veremos cómo podemos utilizar la clase `CountDownLatch` para simular un sistema de videoconferencia. En el sistema se esperará hasta que todos los participantes de la videoconferencia estén disponibles.

1. Definimos una clase llamada `Videoconference` que implementa la interface `Runnable`. Esta clase simulará nuestro sistema de videoconferencia. Crearemos una variable de instancia de la clase `CountDownLatch` y en el constructor se inicializará al número de participantes para la videoconferencia.

```java
...
/**
 * This class implements the controller of the Videoconference
 * 
 * It uses a CountDownLatch to control the arrival of all the 
 * participants in the conference.
 *
 */
public class Videoconference implements Runnable{

    /**
     * This class uses a CountDownLatch to control the arrivel of all
     * the participants
     */
    private final CountDownLatch controller;
	
    /**
     * Constructor of the class. Initializes the CountDownLatch
     * @param number The number of participants in the videoconference
     */
    public Videoconference(int number) {
        controller=new CountDownLatch(number);
    }
...
```

2. Creamos un método llamado `arrive()` que simulará la llegada de un participante a la videoconferencia. Se presentará por la consola el nombre del participante y se invocará al método `countDown()` que indicará que una operación, para la espera de un participante, ha finalizado. Para finalizar se presentará por la consola el número de operaciones que aún faltan por completar.

```java
...
/**
 * This method is called by every participant when he incorporates to the VideoConference
 * @param participant
 */
public void arrive(String name){
    System.out.printf("%s has arrived.\n",name);
    // This method uses the countDown method to decrement the internal counter of the
    // CountDownLatch
    controller.countDown();
    System.out.printf("VideoConference: Waiting for %d participants.\n",controller.getCount());
}
...
```

3. Para finalizar implementamos el método `run()` donde están las sentencias que ejecutará el hilo. Se inicializará el sistema de videoconferencia y antes de empezar con la videoconferencia se esperará a que todos los participantes estén disponibles. Para ello se invocará el método `await()`. Cuando todos los participantes estén disponibles se podrá empezar con la videoconferencia.

```java
...
/**
 * This is the main method of the Controller of the VideoConference. It waits for all
 * the participants and the, starts the conference
 */
@Override
public void run() {
    System.out.printf("VideoConference: Initialization: %d participants.\n",controller.getCount());
    try {
        // Wait for all the participants
        controller.await();
        // Starts the conference
        System.out.printf("VideoConference: All the participants have come\n");
        System.out.printf("VideoConference: Let's start...\n");
     } catch (InterruptedException e) {
        e.printStackTrace();
     }
}
...
```

4. Para simular a los participantes de la videoconferencia definimos una clase llamada `Participant` que implementa la interface `Runnable`. Tendrá como variables de instancia el nombre del participante en la videoconferencia y el sistema de videoconferencia que utiliza. El constructor deberá inicializar el valor para las variables de instancia.

```java
...
/**
 * This class implements a participant in the VideoConference
 *
 */
public class Participant implements Runnable {

    /**
     * VideoConference in which this participant will take part off
     */
    private Videoconference conference;
	
    /**
     * Name of the participant. For log purposes only
     */
    private String name;
	
    /**
     * Constructor of the class. Initialize its attributes
     * @param conference VideoConference in which is going to take part off
     * @param name Name of the participant
     */
    public Participant(Videoconference conference, String name) {
        this.conference=conference;
        this.name=name;
    }
...
```

5. El método `run()` simulará el tiempo que tarda el participante en estar disponible para poder iniciar la videoconferencia. Para indicar que ya está disponible se invoca el método `arrive(.)` del sistema de videoconferencia pasando como parámetro su nombre.

```java
...
/**
 * Core method of the participant. Waits a random time and joins the VideoConference 
 */
@Override
public void run() {
    Long duration=(long)(Math.random()*10);
    try {
        TimeUnit.SECONDS.sleep(duration);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    conference.arrive(name);
}
...
```

6. El método `main(.)` de la aplicación creará una instancia del sistema de videoconferencia en el que participarán 10 personas. Se crea un hilo para lanzar la ejecución del sistema de videoconferencia. Luego se simularán los 10 participantes en la videoconferencia a los que se les asociará un hilo para simular su participación en la videoconferencia.

```java
...
/**
 * Main method of the example
 * @param args
 */
public static void main(String[] args) {

    // Creates a VideoConference with 10 participants.
    Videoconference conference=new Videoconference(10);
    // Creates a thread to run the VideoConference and start it.
    Thread threadConference=new Thread(conference);
    threadConference.start();
		
    // Creates ten participants, a thread for each one and starts them
    for (int i=0; i<10; i++){
        Participant p=new Participant(conference, "Participant "+i);
        Thread t=new Thread(p);
        t.start();
    }
}
...
```

## Información adicional

-   La clase `CountDownLatch` no se utiliza para proteger el acceso compartido a un recurso o **sección crítica**. Su utilidad es para **sincronizar** uno o más hilos con la ejecución de múltiples tareas.
- Su funcionamiento es de un sólo uso, si quisiéramos una nueva sincronización debemos crear un nuevo objeto para poder realizarla.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTAzNzAwMjgyMF19
-->