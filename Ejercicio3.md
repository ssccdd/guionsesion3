# Utilizando múltiples condiciones en un Bloqueo

Como ya hemos visto en ejemplos anteriores puede que sean necesarias condiciones para que un hilo pueda ejecutar la **sección crítica**. Para poder establecer estas condiciones en Java se puede utilizar la interface [Condition](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/Condition.html). Mediante el uso de esta interface tenemos los mecanismos para suspender la ejecución de un hilo así como reanudar la ejecución de un hilo suspendido.

En el ejemplo resolveremos un problema de **Productor/Consumidor**[^nota1] utilizando la interface `Lock` y `Condition`.
  
1. Primero definimos una clase que simulará un fichero de texto. La clase tendrá como nombre `FileMock`. Tendrá dos variables de instancia, un objeto de la clase `String` que simulará el contenido del fichero. Además necesitamos un valor numérico que simulará el índice del fichero que nos indicará la línea de fichero a la que podemos acceder. El constructor simulara un contenido aleatorio para el fichero.

```java
...
/**
 * This class simulates a text file. It creates a defined number
 * of random lines to process them sequentially.
 *
 */
public class FileMock {
	
    /**
     * Content of the simulate file
     */
    private String content[];
    /**
     * Number of the line we are processing
     */
    private int index;
	
    /**
     * Constructor of the class. Generate the random data of the file
     * @param size: Number of lines in the simulate file
     * @param length: Length of the lines
     */
    public FileMock(int size, int length){
        content=new String[size];
        for (int i=0; i<size; i++){
            StringBuilder buffer=new StringBuilder(length);
            for (int j=0; j<length; j++){
                int indice=(int)Math.random()*255;
                buffer.append((char)indice);
            }
            content[i]=buffer.toString();
        }
        index=0;
    }
...
```

2. Implementamos los métodos `hasMoreLines()`, que nos indicará si hemos terminado con el fichero, y el método `getLine()`, que nos devuelve la línea actual del fichero.

```java
...
/**
 * Returns true if the file has more lines to process or false if not
 * @return true if the file has more lines to process or false if not
 */
public boolean hasMoreLines(){
    return index<content.length;
}
	
/**
 * Returns the next line of the simulate file or null if there aren't more lines
 * @return
 */
public String getLine(){
    if (this.hasMoreLines()) {
    System.out.println("Mock: "+(content.length-index));
    return content[index++];
    } 
    return null;
}
...
```

3. Definimos la clase `Buffer` que será el espacio de memoria compartida entre el **productor** y el **consumidor**.

4. Tenemos 6 variables de instancia:
	- Donde el **productor** almacena la información para que el **consumidor** pueda recogerla.
	- Un entero que nos indicará el tamaño máximo del almacenamiento disponible.
	- Un objeto de la clase `ReentrantLock` para controlar la **sección crítica** de acceso al espacio de memoria compartida.
	- Dos variables `Condition`, una pasa saber si hay elementos y otra para saber si se ha alcanzado el límite de almacenamiento.
	- Una variable `boolean` que nos indicará si hemos terminado de procesar el fichero de texto del ejemplo.

5. Creamos un constructor para establecer los valores iniciales de las variables de instancia.

```java
...
/**
 * This class implements a buffer to stores the simulate file lines between the
 * producer and the consumers
 * 
 */
public class Buffer {

/**
 * The buffer
 */
private LinkedList<String> buffer;

/**
 * Size of the buffer
 */
private int maxSize;

/**
 * Lock to control the access to the buffer
 */
private ReentrantLock lock;

/**
 * Conditions to control that the buffer has lines and has empty space
 */
private Condition lines;
private Condition space;

/**
 * Attribute to control where are pending lines in the buffer
 */
private boolean pendingLines;

/**
 * Constructor of the class. Initialize all the objects
 * 
 * @param maxSize
 *            The size of the buffer
 */
public Buffer(int maxSize) {
    this.maxSize = maxSize;
    buffer = new LinkedList<>();
    lock = new ReentrantLock();
    lines = lock.newCondition();
    space = lock.newCondition();
    pendingLines = true;
}
...
```

6. Implementamos el método `insert(.)`. Tendremos como parámetro la línea que deseamos almacenar. Lo primero será establecer el **bloqueo** que garantiza la **exclusión mutua** de esta operación. Comprobamos si hay espacio para almacenar la línea y en caso contrario invocamos el método `await()`  sobre la condición `space`  que suspenderá la ejecución del hilo. El siguiente paso es almacenar la línea y mostrar un mensaje en la consola indicando la acción. Luego reanudaremos los hilos que estén bloqueados en la condición `lines` mediante el método `signalAll()`. Para finalizar liberaremos el bloqueo de la **exclusión mutua** mediante la cláusula `finally` que evitará los **deadlock** por si se interrumpe la ejecución del hilo antes de acabar con su tarea.

```java
...
/**
 * Insert a line in the buffer
 * 
 * @param line
 *            line to insert in the buffer
 */
public void insert(String line) {
    lock.lock();
    try {
        while (buffer.size() == maxSize) {
            space.await();
        }
        buffer.offer(line);
        System.out.printf("%s: Inserted Line: %d\n", Thread.currentThread()
					.getName(), buffer.size());
        lines.signalAll();
    } catch (InterruptedException e) {
        e.printStackTrace();
    } finally {
        lock.unlock();
    }
}
...
```

7. Implementamos el método `get()` para devolver una línea almacenada. Como en el caso anterior, establecemos el **bloqueo** que garantiza la **exclusión mutua** de esta operación. Comprobamos si hay líneas almacenadas y aún no hemos acabado de procesar el fichero de texto. Si no podemos retirar una línea y no hemos terminado se invocara el método `await()` en la condición `lines` que suspenderá la ejecución del hilo. Si no hemos terminado el procesamiento del fichero, retiramos una línea y lo comunicamos por la consola. Reanudaremos los hilos bloqueados en la condición `space` mediante el método `signalAll()`. Para finalizar liberaremos el **bloqueo** de la **exclusión mutua** mediante la cláusula `finally` que evitará los **deadlock** por si se interrumpe la ejecución del hilo antes de acabar con su tarea.

```java
...
/**
 * Returns a line from the buffer
 * 
 * @return a line from the buffer
 */
public String get() {
    String line=null;
    lock.lock();		
    try {
        while ((buffer.size() == 0) &&(hasPendingLines())) {
            lines.await();
        }
			
        if (hasPendingLines()) {
            line = buffer.poll();
            System.out.printf("%s: Line Readed: %d\n",Thread.currentThread().getName(),buffer.size());
            space.signalAll();
        }
    } catch (InterruptedException e) {
        e.printStackTrace();
    } finally {
        lock.unlock();
    }
    return line;
}
...
```

8. Definimos la clase que representará al **productor** y la nombramos `Producer` que implementa la interface `Runnable`. Tendrá dos variables de instancia, una para el espacio de memoria compartida y otra para el fichero simulado. El constructor establecerá los valores iniciales de estas variables.

```java
...
/**
 * This class gets lines from the simulate file and stores them in the
 * buffer, if there is space in it.
 *
 */
public class Producer implements Runnable {

    /**
     * Simulated File
     */
    private FileMock mock;
	
    /**
     * Buffer
     */
    private Buffer buffer;
	
    /**
     * Constructor of the class. Initialize the objects
     * @param mock Simulated file
     * @param buffer Buffer
     */
    public Producer (FileMock mock, Buffer buffer){
        this.mock=mock;
        this.buffer=buffer;	
    }
...
```

9. Implementamos el método `run()` que retirará líneas del fichero de texto y las incluirá en el espacio de memoria compartida.

```java
...
/**
 * Core method of the producer. While are pending lines in the
 * simulated file, reads one and try to store it in the buffer.
 */
@Override
public void run() {
    buffer.setPendingLines(true);
    while (mock.hasMoreLines()){
        String line=mock.getLine();
        buffer.insert(line);
    }
    buffer.setPendingLines(false);
}
...
```

10. Definimos la clase que representa al **consumidor** y la llamaremos `Consumer` que implementa la interface `Runnable`. Tenemos una variable de instancia para el espacio de memoria compartida. El constructor inicializa el valor de la variable de instancia.

```java
...
/**
 * This class reads line from the buffer and process it
 *
 */
public class Consumer implements Runnable {

    /**
     * The buffer
     */
    private Buffer buffer;
	
    /**
     * Constructor of the class. Initialize the buffer
     * @param buffer
     */
    public Consumer (Buffer buffer) {
        this.buffer=buffer;
    } 
...
```

11. Implementamos el método `run()` que irá retirando una línea del espacio de memoria compartida hasta que se haya procesado todo el fichero de texto. Por último implementaremos un método `processLine(.)` que simulará el tiempo que podría llevar el tratamiento de esa línea por el hilo.


```java
...
/**
 * Core method of the consumer. While there are pending lines in the
 * buffer, try to read one.
 */
@Override
public void run() {
    while (buffer.hasPendingLines()) {
        String line=buffer.get();
        processLine(line);
    }
}

/**
 * Method that simulates the processing of a line. Waits 10 milliseconds
 * @param line
 */
private void processLine(String line) {
    try {
        Random random=new Random();
        Thread.sleep(random.nextInt(100));
    } catch (InterruptedException e) {
        e.printStackTrace();
    }		
}
...
```

12. Para finalizar, implementamos el método `main(.)` de la aplicación. Tenemos que crear un objeto para el fichero simulado, un objeto para el espacio de memoria compartida, un objeto para representar al productor que lo asociaremos al objeto de espacio de memoria compartida, tres objetos para representar a 3 consumidores a los que asociaremos el objeto de memoria compartida. Crearemos objetos de la clase `Thread` para asociarlos al **productor** y a los **consumidores**. Lo último será ejecutar los hilos.

```java
...
public static void main(String[] args) {
    /**
     * Creates a simulated file with 100 lines
     */
    FileMock mock=new FileMock(101, 10);
		
    /**
     * Creates a buffer with a maximum of 20 lines
     */
    Buffer buffer=new Buffer(20);
		
    /**
     * Creates a producer and a thread to run it
     */
    Producer producer=new Producer(mock, buffer);
    Thread threadProducer=new Thread(producer,"Producer");
		
    /**
     * Creates three consumers and threads to run them
     */
    Consumer consumers[]=new Consumer[3];
    Thread threadConsumers[]=new Thread[3];
		
    for (int i=0; i<3; i++){
        consumers[i]=new Consumer(buffer);
        threadConsumers[i]=new Thread(consumers[i],"Consumer "+i);
    }
		
    /**
     * Strats the producer and the consumers
     */
    threadProducer.start();
    for (int i=0; i<3; i++){
        threadConsumers[i].start();
    }
}
...
```

## Ejercicios propuestos

-   Revisar el libro *Java 7 Concurrency Cookbook*[^nota2] para tener una explicación más detallada sobre el resultado del ejemplo.    
-   Realizar una aplicación para simular un sistema de impresión donde hay un espacio común donde diferentes usuarios almacenan sus trabajos que posteriormente serán impresos por una impresora. Dispondremos de 4 impresoras para atender los trabajos de impresión.


---
[^nota1]: Este problema será expuesto en detalle en las clases de teoría.
[^nota2]: El libro se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTM5NTU3NzEwN119
-->