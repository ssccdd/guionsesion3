# Sincronizando el acceso a los datos mediante la interface `ReadWriteLock`

Una mejora significativa que se obtiene con la utilización del mecanismo presentado en el ejemplo anterior es el uso de la interface [`ReadWriteLock`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/ReadWriteLock.html) mediante la clase [`ReentrantReadWriteLock`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/ReentrantReadWriteLock.html) que la implementa. Con esta clase podremos separar las acciones de lectura de las de escritura, problema del **Lector/Escritor**[^nota1]. De esta forma múltiples hilos podrán realizar operaciones de lectura concurrentemente pero sólo podrá haber un único hilo realizando operaciones de escritura, tampoco podrá haber otros hilos realizando operaciones de lectura si hay una operación de escritura en marcha.

En el ejemplo aprenderemos a utilizar la interface `ReadWriteLock` en un programa que utiliza el acceso a un objeto para almacenar el precio de dos productos.

1. Definimos una clase con el nombre `PricesInfo` que tendrá dos variables de instancia para almacenar el precio de dos productos. Además tendremos otra variable de instancia para un objeto que implemente la interface `ReadWriteLock`. Todas las variables de instancia las inicializamos mediante el constructor.

```java
...
/**
 * This class simulates the store of two prices. We will
 * have a writer that stores the prices and readers that 
 * consult this prices
 *
 */
public class PricesInfo {
	
    /**
     * The two prices
     */
    private double price1;
    private double price2;
	
    /**
     * Lock to control the access to the prices
     */
    private ReadWriteLock lock;
	
    /**
     * Constructor of the class. Initializes the prices and the Lock
     */
    public PricesInfo(){
        price1=1.0;
        price2=2.0;
        lock=new ReentrantReadWriteLock();
    }
...
```

2. Implementamos los métodos que nos permitirán acceder al precio de los productos, necesitaremos uno para cada precio. Utilizaremos el **bloqueo** para lectura de la interface antes de leer el precio y lo liberaremos una vez que hemos terminado. Para finalizar devolveremos el valor.

```java
...
/**
 * Returns the first price
 * @return the first price
 */
public double getPrice1() {
    lock.readLock().lock();
    double value=price1;
    lock.readLock().unlock();
    return value;
}
...
```

3. Implementaremos un método llamado `setPrices(.)`  para establecer el precio de los dos productos. Lo primero será obtener el **bloqueo** para escritura de la interface antes de establecer el precio de los productos. Para finalizar liberaremos el **bloqueo**.

```java
...
/**
 * Establish the prices
 * @param price1 The price of the first product
 * @param price2 The price of the second product
 */
public void setPrices(double price1, double price2) {
    lock.writeLock().lock();
    this.price1=price1;
    this.price2=price2;
    lock.writeLock().unlock();
}
...
```

4. El siguiente paso es definir las clases que representarán a los **lectores** y **escritores**. Primero definimos una clase llamada `Reader` que implementa la interface `Runnable`. Tiene una variable de instancia para almacenar un objeto de la clase `PriecesInfo` que se inicializa con el constructor.

```java
...
/**
 * This class implements a reader that consults the prices
 *
 */
public class Reader implements Runnable {

    /**
     * Class that stores the prices
     */
    private PricesInfo pricesInfo;
	
    /**
     * Constructor of the class
     * @param pricesInfo object that stores the prices
     */
    public Reader (PricesInfo pricesInfo){
        this.pricesInfo=pricesInfo;
    }
...
```

5. El método `run()` realiza diferentes accesos para leer los precios de ambos productos y los mostrará por la consola.

```java
...
/**
 * Core method of the reader. Consults the two prices and prints them
 * to the console
 */
@Override
public void run() {
    for (int i=0; i<10; i++){
        System.out.printf("%s: Price 1: %f\n",Thread.currentThread().getName(),pricesInfo.getPrice1());
        System.out.printf("%s: Price 2: %f\n",Thread.currentThread().getName(),pricesInfo.getPrice2());
    }
}
...
```

6. Ahora definimos una clase con el nombre `Writer` que implementa la interface `Runnable`. Tiene como variable de instancia un objeto de la clase `PricesInfo` que inicializamos con el constructor.

```java
...
/**
 * This class implements a writer that establish the prices
 *
 */
public class Writer implements Runnable {

    /**
     * Class that stores the prices
     */
    private PricesInfo pricesInfo;
	
    /**
     * Constructor of the class
     * @param pricesInfo object that stores the prices
     */
    public Writer(PricesInfo pricesInfo){
        this.pricesInfo=pricesInfo;
    }
...
```

7. El método `run()` realiza tres veces la modificación del precio de los productos. Mostrará un mensaje antes de modificarlos y otro cuando los ha modificado.

```java
...
/**
 * Core method of the writer. Establish the prices
 */
@Override
public void run() {
    for (int i=0; i<3; i++) {
        System.out.printf("Writer: Attempt to modify the prices.\n");
        pricesInfo.setPrices(Math.random()*10, Math.random()*8);
        System.out.printf("Writer: Prices have been modified.\n");
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
...
```

8. Implementamos el método `main(.)` de la aplicación. Tendremos un objeto de la clase `PricesInfo` que será necesario para construir los objetos de la clase `Reader` y `Writer`. Creamos 5 objetos para simular los **lectores** y uno para el **escritor**.

9. Crearemos objetos de la clase `Thread` para asociarlos a los objetos de los **lectores** y del **escritor**. Para finalizar iniciaremos la ejecución de los hilos.

## Ejercicios propuestos

-   Revisar el libro *Java 7 Concurrency Cookbook*[^nota2] para tener una explicación más detallada sobre el resultado del ejemplo.
-   Modificar el ejemplo para que pueda trabajar con más escritores. También modificar la clase `Reader` para que realice más lecturas y así comprobar el resultado.
-   Con las modificaciones anteriores limitar la ejecución del programa principal a 5 segundos, cancelando las operaciones que no hayan concluido.
- Revisar en el libro *Java 7 Concurrency Cookbook* el ejemplo donde se puede cambiar el comportamiento de las clases `ReentrantLock` y `ReentrantReadWriteLock`.

---
[^nota1]: Este problema será expuesto en detalle en las clases de teoría.
[^nota2]: El libro se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTUzMjg0OTAyNV19
-->