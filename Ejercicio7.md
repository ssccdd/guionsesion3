# Controlando el cambio de etapa en tareas por etapas concurrentes

La clase [`Phaser`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html) proporcionan un método que se ejecuta cada vez que se cambia de etapa. El método es `onAdvance(.)`, tiene dos parámetros: el número de la etapa actual y el número de participantes registrados; devuelve un `boolean`, `false` si aún no se han completado las etapas, o `true` si todas las etapas han finalizado y se debe pasar al estado de terminación. La implementación por defecto que tiene el método devuelve `true` si el número de participantes registrados es `0` y `false` en otro caso. Podemos modificar ese comportamiento si definimos una clase que herede de `Phaser` e implementamos el método `onAdvance(.)`. Normalmente, estaremos interesados en hacer esto cuando tengamos que ejecutar algunas acciones cuando queramos pasar de una etapa a otra.

En este ejemplo aprenderemos cómo podemos controlar el cambio de etapas y para realizar esto definimos una clase que herede de `Phaser` para implementar el método `onAdvance(.)` y así ejecutar algunas acciones cuando pasemos de etapa. Nuestro ejemplo consistirá en simular la realización de una examen donde los alumnos deberán completar tres ejercicios. Todos los estudiantes deberán completar un ejercicio antes de pasar al siguiente.

1. Como primer paso se define una clase llamada `MyPhaser` que heredará de `Phaser`.

2. Implementamos el método `onAdvance(.)` para que dependiendo de la etapa que nos encontremos invoquemos diferentes métodos auxiliares de la clase. Las etapas se dividirán en: `0`, esperamos hasta que estén todos los estudiantes, `1` finalización del primer ejercicio, `2` finalización del segundo ejercicio, `3` finalización del examen. En cualquier otro caso habremos finalizado y para ello devolveremos `true`.

```java
...
/**
 * This method is called when the last register thread calls one of the advance methods
 * in the actual phase
 * @param phase Actual phase
 * @param registeredParties Number of registered threads
 * @return false to advance the phase, true to finish
 */
@Override
protected boolean onAdvance(int phase, int registeredParties) {
    switch (phase) {
        case 0:
            return studentsArrived();
        case 1:
            return finishFirstExercise();
        case 2:
            return finishSecondExercise();
        case 3:
            return finishExam();
        default:
            return true;
    }
}
...
```

3. Implementaremos cada uno de los métodos auxiliares para simular acciones a realizar cada vez que se complete una de las etapas. Como paso final de cada uno de los métodos devolverá `false` dado que aún quedan etapas por finalizar. Para revisar el código, consultar el ejemplo de la sesión.

4. Definimos una clase llamada `Student` que implementa la interface `Runnable` para simular las acciones que deberá realizar un estudiante para completar el examen. Tendrá una variable de instancia con una referencia de la clase `MyPhaser` para realizar las acciones necesarias cada vez que todos los alumnos han completado una etapa. El código lo tenéis disponible en el ejemplo de la sesión.

5. Las acciones que deberá realizar el estudiante se encuentran implementadas en el método `run()`. Donde se indicará cuando un alumno está disponible para realizar el primer ejercicio del examen y cuando ha completado cada uno de los ejercicios. Para esperar a que todos los alumnos completen la etapa correspondiente se invocará al método `arriveAndAwaitAdvance()`.

```java
...
/**
 * Main method of the student. It arrives to the exam and does three exercises. After each
 * exercise, it calls the phaser to wait that all the students finishes the same exercise
 */
public void run() {
    System.out.printf("%s: Has arrived to do the exam. %s\n",Thread.currentThread().getName(),new Date());
    phaser.arriveAndAwaitAdvance();
    System.out.printf("%s: Is going to do the first exercise. %s\n",Thread.currentThread().getName(),new Date());
    doExercise1();
    System.out.printf("%s: Has done the first exercise. %s\n",Thread.currentThread().getName(),new Date());
    phaser.arriveAndAwaitAdvance();
    System.out.printf("%s: Is going to do the second exercise. %s\n",Thread.currentThread().getName(),new Date());
    doExercise2();
    System.out.printf("%s: Has done the second exercise. %s\n",Thread.currentThread().getName(),new Date());
    phaser.arriveAndAwaitAdvance();
    System.out.printf("%s: Is going to do the third exercise. %s\n",Thread.currentThread().getName(),new Date());
    doExercise3();
    System.out.printf("%s: Has finished the exam. %s\n",Thread.currentThread().getName(),new Date());
    phaser.arriveAndAwaitAdvance();
}
...
```

6. Para cada una de las pruebas se crearán métodos auxiliares que simularán el tiempo que le lleva a cada alumno completar el ejercicio. El código se encuentra disponible en el ejemplo de la sesión.

7. El método `main(.)` de la aplicación debe crear la instancia de la clase `MyPhaser` que será pasada al constructor de cada uno de los 5 estudiantes simulados. Se crearán los hilos necesarios para ejecutar los 5 estudiantes y el programa principal esperará hasta que todos hayan finalizado el examen. El código se encuentra disponible en el ejemplo de la sesión.

## Ejercicios propuestos

- Revisar los métodos de la clase [`Phaser`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html) para comprender el significado y utilidad de cada uno de ellos.
- Modificar el ejemplo anterior para simular que un 10% de los estudiantes no supera cada una de las pruebas.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTkzMTU3MzUxOV19
-->