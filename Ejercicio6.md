# Ejecución de tareas por etapas concurrentes

Una de las más complejas y potentes funcionalidades que Java nos proporciona es la ejecución de tareas por etapas concurrentes mediante la clase [`Phaser`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html). Este mecanismo es útil cuando tenemos algunas tareas concurrentes divididas en etapas. La clase `Phaser` nos proporciona el mecanismo para sincronizar los hilos al final de cada una de las etapas, esto es, los hilos no ejecutarán su segunda etapa hasta que todos los hilos hayan completado su primera etapa. Como con otras utilidades de sincronización, tenemos que utilizar el constructor de la clase `Phaser` con el número de tareas que participan en la sincronización, pero podemos modificar ese número de forma dinámica incrementando o decrementando su valor durante la ejecución de los hilos.

En el ejemplo veremos la utilización de la clase `Phaser` para la sincronización de tareas concurrentes. Tendremos tres tareas que buscan ficheros con la extensión `.log` modificados en las últimas **24h** en tres directorios diferentes y sus subdirectorios. Estas tareas se dividirán en tres etapas:

1.  Obtendremos la lista de los ficheros con la extensión `.log` en el directorio asignado y sus subdirectorios.
2.  Filtrar la lista anterior eliminando aquellos que han sido modificados hace más de **24h**.
3.  Presentar el resultado en la consola.
    
Al final de la etapa 1 y 2 si hay elementos se muestran en la etapa 3. Si no hay ningún elemento, los hilos finalizan su ejecución y son eliminados de la clase `Phaser`.

1. Definimos una clase llamada `FileSearch` que implementa la interface `Runnable`. En esta clase tendremos las operaciones de búsqueda para los ficheros con una determinada extensión modificados en las últimas **24h** en un directorio y sus subdirectorios. Las variables de instancia necesarias serán:
	- La que indicará el directorio donde se iniciará la búsqueda.
	- Donde se almacenará la extensión de los ficheros que se buscarán.
	- Una lista donde se almacenarán las trayectorias completas de los ficheros encontrados.
	- Un objeto de la clase `Phaser` para la sincronización de las diferentes etapas de las tareas.

En el ejemplo de la sesión se puede revisar el código necesario para las variables de instancia así como el constructor para los valores iniciales de estas variables de instancia.

2. Implementamos una serie de métodos auxiliares que serán necesarios para completar las tareas en el método `run()`. El primero de ellos se llamará `directoryProcess(.)`. Tendrá como parámetro un objeto de la clase `File` y obtendrá todos los ficheros para el directorio y sus subdirectorios de forma *recursiva*. Para los ficheros se los pasará al método `fileProcess(.)`.

```java
...
/**
 * Method that process a directory
 * 
 * @param file
 *            : Directory to process
 */
private void directoryProcess(File file) {

    // Get the content of the directory
    File list[] = file.listFiles();
    if (list != null) {
        for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
                // If is a directory, process it
                directoryProcess(list[i]);
            } else {
                // If is a file, process it
                fileProcess(list[i]);
            }
        }
    }
}
...
```

3. Implementamos el método `fileProcess(.)` que tiene como parámetro un fichero y comprobará si su extensión coincide con la que se está buscando. Si es así la almacenará en la variable de instancia correspondiente al resultado de la búsqueda.

```java
...
/**
 * Method that process a File
 * 
 * @param file
 *            : File to process
 */
private void fileProcess(File file) {
    if (file.getName().endsWith(end)) {
        results.add(file.getAbsolutePath());
    }
}
...
```

4. Implementamos el método `filterReults()` que se quedará sólo con los ficheros que se han modificado en las últimas **24h**.

```java
...
/**
 * Method that filter the results to delete the files modified more than a day before now
 */
private void filterResults() {
    List<String> newResults=new ArrayList<>();
    long actualDate=new Date().getTime();
    for (int i=0; i<results.size(); i++){
        File file=new File(results.get(i));
        long fileDate=file.lastModified();
			
        if (actualDate-fileDate<TimeUnit.MILLISECONDS.convert(1,TimeUnit.DAYS)){
            newResults.add(results.get(i));
        }
    }
    results=newResults;
}
...
```

5. Implementamos ahora el método `checkResults()` que se invocará al final de la primera y segunda etapa para saber si hay resultados de la búsqueda. Si no hubiera resultados presenta un mensaje en la consola indicando esta circunstancia e invocará el método `arriveAndDeregister()` de la clase `Phaser` para indicar que ha completado la etapa y que abandona las tareas para el resto de las etapas.
  
6. En otro caso presentará un mensaje en la consola e invocará al método `arriveAndAwaitAdvance()` para indicar que ha completado la etapa y que se bloqueará hasta que el resto de hilos complete la etapa actual.

```java
...
/**
 * This method checks if there are results after the execution of a phase. If there aren't
 * results, deregister the thread of the phaser.
 * @return true if there are results, false if not
 */
private boolean checkResults() {
    if (results.isEmpty()) {
        System.out.printf("%s: Phase %d: 0 results.\n",Thread.currentThread().getName(),phaser.getPhase());
        System.out.printf("%s: Phase %d: End.\n",Thread.currentThread().getName(),phaser.getPhase());
        // No results. Phase is completed but no more work to do. Deregister for the phaser
        phaser.arriveAndDeregister();
        return false;
    } else {
        // There are results. Phase is completed. Wait to continue with the next phase
        System.out.printf("%s: Phase %d: %d results.\n",Thread.currentThread().getName(),phaser.getPhase(),results.size());
        phaser.arriveAndAwaitAdvance();
        return true;
    }
}
...
```

7. El último método auxiliar se llama `showInfo()` que presentará en la consola el resultado de la búsqueda.

```java
...
/**
 * This method prints the final results of the search
 */
private void showInfo() {
    for (int i=0; i<results.size(); i++){
        File file=new File(results.get(i));
        System.out.printf("%s: %s\n",Thread.currentThread().getName(),file.getAbsolutePath());
    }
    // Waits for the end of all the FileSearch threads that are registered in the phaser
    phaser.arriveAndAwaitAdvance();
}
...
```

8.- Ahora implementamos el método `run()` que contendrá las tareas principales que debemos completar para la búsqueda. Lo primero será invocar el método `awaitAndAdvance()` para que todos los hilos comiencen las tareas de búsqueda al mismo tiempo.

```java
...
/**
 * Main method of the class. See the comments inside to a better description of it
 */
@Override
public void run() {
		
    // Waits for the creation of all the FileSearch objects
    phaser.arriveAndAwaitAdvance();
    System.out.printf("%s: Starting.\n",Thread.currentThread().getName());
...
```

9. Empezamos con la primera etapa. Comprobamos que el directorio existe y en caso afirmativo obtenemos las lista de fichero con la extensión que estamos buscando. Al finalizar esta etapa comprobamos si disponemos de datos.

```java
...
// 1st Phase: Look for the files
File file = new File(initPath);
if (file.isDirectory()) {
    directoryProcess(file);
}
		
// If no results, deregister in the phaser and ends
if (!checkResults()){
    return;
}
...
```

10. Continuamos con la segunda etapa. Nos quedamos con los ficheros que hayan sido modificados en las últimas **24h**. Al finalizar la etapa comprobamos si disponemos de datos.

```java
...
// 2nd Phase: Filter the results
filterResults();
		
// If no results after the filter, deregister in the phaser and ends
if (!checkResults()){
    return;
}
...
```

11. La última etapa consiste en presentar los datos en la consola e indicar se ha finalizado con el trabajo.
 
```java
...
// 3rd Phase: Show info
showInfo();
phaser.arriveAndDeregister();
System.out.printf("%s: Work completed.\n",Thread.currentThread().getName());
...
```

12. En el método `main()` de la aplicación crearemos un objeto de la clase `Phaser` y lo inicializamos a 3 que serán los hilos que participan en la búsqueda. Creamos tres objetos de la clase `FileSearch` con los elementos necesarios para la búsqueda y se los asociamos a los hilos que se ejecutarán. El programa principal esperará hasta que finalicen todos ellos. Revisar el código en el ejemplo de la sesión.

## Ejercicios propuestos

- Revisar el [API](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Phaser.html) para comprobar los métodos disponibles para la clase `Phaser`
- Modificar el ejemplo para elegir los directorios de búsqueda y la extensión. Además se debe poder elegir el número de hilos que realizan la búsqueda.

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIxMjE5NDU5NDddfQ==
-->