# Sincronizando tareas en un punto en común

Java nos proporciona una utilidad de sincronización que permite sincronizar uno o más hilos en un punto en común, mediante la clase [`CyclicBarrier`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CyclicBarrier.html). Es una clase similar a la presentada en el ejemplo anterior, pero presenta algunas características nuevas que la convierten en una clase más potente.

El constructor también utiliza un número entero, que es el número de hilos que se sincronizan en un punto en común. Cuando un hilo llega a ese punto invoca al método `await()` para esperar esperar a los otros hilos. Los hilos se bloquearán hasta que el último hilo, por el que se debe esperar, invoque el método `await()`. En este momento se reanudará la ejecución de todos los hilos que invocaron el método `await()` previamente.

Una característica nueva de `CyclicBarrier` es que se puede pasar en el constructor un objeto que implemente la interface `Runnable` que lo ejecutará por medio de un hilo cuando el resto de hilos lleguen al punto en común. Esta característica convierte a esta clase como una solución adecuada para la paralización de las tareas en una estrategia de **divide y vencerás**.

En el ejemplo veremos como utilizar la clase `CyclicBarrier` para sincronizar un conjunto de hilos en un punto determinado. También se utilizará un objeto que implementa la interface `Runnable` que se ejecutará una vez que todos los hilos lleguen al punto de sincronización. En el ejemplo buscaremos un número en una matriz. La matriz la dividiremos en subconjuntos (mediante la técnica del **divide y vencerás**), donde cada hilo buscará por el número en ese subconjunto. Cuando todos los hilos terminen su trabajo, una tarea final unifica los resultados.

1. Para empezar creamos una clase llamada `MatrixMock` que se rellenará con una secuencia de números aleatorios entre 1 y 10. El código está disponible en el ejemplo de la sesión.

2. Definimos un método llamado `getRow(.)` al que le pasaremos como parámetro un número de fila de la matriz y devolverá la fila si existe y `null` si no es una fila de la matriz.

```java
...
/**
 * This methods returns a row of the bi-dimensional array
 * @param row the number of the row to return
 * @return the selected row
 */
public int[] getRow(int row){
    if ((row>=0)&&(row<data.length)){
        return data[row];
    }
    return null;
}
...
```

3. Ahora definimos una nueva clase llamada `Results`. Esta clase almacenará, en un array, el número de ocurrencias del número que se busca en cada fila de la matriz. Para ello necesitaremos dos métodos, uno llamado `setData(.)` para incluir un valor en el array. El otro método se llama `getData()` y nos devolverá el array con los resultados. El código se puede revisar en el ejemplo de la sesión.

4. Ya hemos definido las clases auxiliares que utilizaremos para nuestro ejemplo. Ahora definimos una clase llamada `Searcher` que implementa la interface `Runnable`. Esta clase buscará un número en una de las filas de la matriz. Las variables de instancia para la clase serán:
	- La fila inicial de la matriz donde buscaremos.
	- La fina final de la matriz donde buscaremos.
	- La matriz que compone el espacio de búsqueda.
	- Una instancia de la clase `Results` donde se almacenará el resultado.
	- Una instancia de del clase `CyclicBarrier` para poder sincronizarse con el resto de hilos que realizan la búsqueda.

El constructor inicializa los valores apropiados para las variables de instancia. Revisar el código en el ejemplo de la sesión.

5. El método `run()` realizará el trabajo de búsqueda. Tendrá una variable que llevará las ocurrencias del número que se busca por cada una de las filas. Recorrerá cada una de las filas que tiene asignadas: inicializará el contador de ocurrencias a 0, obtendrá la fila, buscará el número y aumentará el contador en 1 cada vez que lo encuentre y para finalizar almacenará el resultado. Cuando haya terminado lo comunicará por la consola e invocará el método `await()` para esperar a que todos los hilos terminen con su tarea de búsqueda.

```java
...
/**
 * Main method of the searcher. Look for the number in a subset of rows. For each row, saves the
 * number of occurrences of the number in the array of results
 */
@Override
public void run() {
    int counter;
    System.out.printf("%s: Processing lines from %d to %d.\n",Thread.currentThread().getName(),firstRow,lastRow);
    for (int i=firstRow; i<lastRow; i++){
        int row[]=mock.getRow(i);
        counter=0;
        for (int j=0; j<row.length; j++){
            if (row[j]==number){
                counter++;
            }
        }
        results.setData(i, counter);
    }
    System.out.printf("%s: Lines processed.\n",Thread.currentThread().getName());
    try {
        barrier.await();
    } catch (InterruptedException e) {
        e.printStackTrace();
    } catch (BrokenBarrierException e) {
        e.printStackTrace();
    }
}
...
```

8. Ahora definimos la clase que se ejecutará una vez que se concluye la tarea de búsqueda. La clase se llama `Grouper` e implementa la interface `Runnable`. Tendrá como variable de instancia el objeto donde se almacenarán los resultados de la búsqueda. El código se puede revisar en el ejemplo de la sesión.

9. El método `run()` procesará el objeto donde se encuentran almacenados los resultados de la búsqueda. Para ello sumará el número de ocurrencias que cada unos de las búsquedas ha obtenido para dar el resultado final.

```java
...
/**
 * Main method of the Grouper. Sum the values stored in the Results object 
 */
@Override
public void run() {
    int finalResult=0;
    System.out.printf("Grouper: Processing results...\n");
    int data[]=results.getData();
    for (int number:data){
        finalResult+=number;
    }
    System.out.printf("Grouper: Total result: %d.\n",finalResult);
}
...
```

8. El método `main(.)` de la aplicación creará e inicializará la matriz de búsqueda. Creará una instancia para almacenar el resultado, el objeto para agrupar los resultados de búsqueda y el objeto de la clase `CyclicBarrier` que se utilizará para la sincronización de la búsqueda. Además creará los hilos que realizan la búsqueda. El código se puede revisar en el ejemplo de la sesión.

## Información adicional

La clase `CyclicBarrier` dispone además de los siguientes métodos que pueden ser útiles:

- `getNunberWaiting()`: Nos devuelve el número de hilos que estás bloqueados por el método `await()`.

- `reset()`: Permite inicializar el estado otra vez, a diferencia de la clase `CountDownLatch`.
 
- Revisar el [API de Java 8](https://docs.oracle.com/javase/8/docs/api/) para una mejor información.
    
## Ejercicios propuestos

- Realizar una modificación del ejemplo anterior para que calcule la suma de la matriz.
 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjcxNjI0NzBdfQ==
-->